Travel Planet Auth0
Stable: v0.1

Requirements (Already covered with Docker deployment)

Apache/2.4.27 or greater.
MySQL 5.7 or greater.
PHP/7.2.24 or greater.



Deploy with Docker (Linux/Debian, Apache, MySQL, PHP)

Run docker-compose up -d.
Install Docker Compose.
Look for PHP/Apache Container ID with docker ps.
Bash for the next steps => App configuration
with docker exec -it {PHP/Apache Container ID} bash.



App Configuration

Add host travel.localhost,
see Edit hosts.
Create .env file from example.env and set it.
Set db instead localhost in .env while using Docker.
Give Folder permissions:
sudo chown -R $USER:www-data storage;
chmod -R 775 storage;
sudo chown -R $USER:www-data bootstrap/cache;
chmod -R 775 bootstrap/cache;

Import database from database/updates/database.sql into travel DB
with root user, at localhost host, 33063 port.
Set APP_KEY=base64:Z/P5j1VV/3peijg/WLWTEz5rU6JHcUdeb0RiIV8Tp/0= at .env.
Run composer install.
Run php artisan storage:link.
Run php artisan migrate.



Load Fake Data (For Development and testing)

Copy and merge content from fake-data/public to storage/app/public.
Import database from fake-data/*.sql into travel DB
with root user, at localhost host, 33063 port.





2020 Lum labs
